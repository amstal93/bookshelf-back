FROM openjdk:11.0.7-jdk-slim as customjre

ARG JAR_FILE

WORKDIR /app
COPY target/${JAR_FILE} app.jar

RUN jlink --output /app/customjre --add-modules $(jdeps --print-module-deps /app/app.jar),java.xml,jdk.unsupported,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument

FROM debian:9-slim

RUN apt-get update && apt-get install -y curl jq

WORKDIR /app
COPY --from=customjre /app/app.jar app.jar
COPY --from=customjre /app/customjre /app/customjre

HEALTHCHECK --interval=10s --timeout=15s --start-period=30s \
  CMD curl --silent --fail http://localhost:8080/actuator/health | jq --exit-status '.status == "UP"' || exit 1

EXPOSE 8080/tcp

ENTRYPOINT ["/app/customjre/bin/java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=prod,actuator","-jar","/app/app.jar"]
