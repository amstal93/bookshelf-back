select row_number() over ()              AS `id`,
       `a`.`id`                          AS `artist_id`,
       `a`.`name`                        AS `name`,
       `s`.`name`                        AS `series`,
       `r`.`name`                        AS `roles`,
       count(`ca`.`contracts_book_isbn`) AS `count`
from ((((`bookshelf`.`artist` `a` join `bookshelf`.`contract_artists` `ca` on ((`a`.`id` = `ca`.`artists_id`))) join `bookshelf`.`book` `b` on ((`ca`.`contracts_book_isbn` = `b`.`isbn`))) join `bookshelf`.`role` `r` on ((`ca`.`contracts_role_id` = `r`.`id`)))
    join `bookshelf`.`series` `s` on ((`b`.`series_id` = `s`.`id`)))
group by `a`.`id`, `a`.`name`, `s`.`name`, `r`.`name`;
