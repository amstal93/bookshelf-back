create trigger series_update
    after update
    on book
    for each row
BEGIN
    IF (OLD.status != 'READ' AND NEW.status = 'READ') THEN
        UPDATE series s SET s.last_read_book_date = NEW.last_update_date where NEW.series_id = s.id;
    END IF;
END;
