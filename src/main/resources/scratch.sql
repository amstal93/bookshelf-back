SELECT @@sql_mode;
SET sql_mode = (SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));
SELECT @@GLOBAL.sql_mode;
SET GLOBAL sql_mode = (SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));

select artist.*
from (
         select vasrc.*,
                row_number() OVER (
                    partition by vasrc.artist_id
                    ORDER BY IF(vasrc.roles = '', 0, 1), IF(vasrc.series = '', 0, 1), vasrc.count desc, vasrc.name DESC
                    ) AS rn
         from v_artist_series_role_count as vasrc
     ) sub
         join artist on artist_id = artist.id
WHERE rn = 1
ORDER BY IF(sub.roles = :role, 0, 1), IF(sub.series = :series, 0, 1), sub.count desc, sub.name DESC;

select artist.*, sub.*
from (
         select vn.*,
                row_number() OVER (
                    partition by vn.artist_id
                    ORDER BY vn.count desc, vn.name DESC
                    ) AS rn
         from view_name as vn
     ) sub
         join artist on artist_id = artist.id
WHERE artist.name like '%%' AND rn = 1
ORDER BY sub.count desc, sub.name DESC;

select *
from (
         select vn.*,
                row_number() OVER (
                    partition by vn.artist_id
                    ORDER BY
                        IF(roles = :roles, 0, 1),
                        IF(series = :series, 0, 1),
                        vn.count DESC,
                        vn.name DESC
                    ) AS rn
         from view_name as vn
     ) sub
         join artist on artist_id = artist.id
WHERE artist.name like :name AND rn = 1
ORDER BY
    IF(sub.roles = :roles, 0, 1),
    IF(sub.series = :series, 0, 1),
    sub.count DESC,
    sub.name DESC;
