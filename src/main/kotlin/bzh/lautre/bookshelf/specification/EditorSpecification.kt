package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.model.Series
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

class EditorSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Editor>(criteria) {

    override fun toPredicate(root: Root<Editor>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        query.distinct(true)
        return when (this.criteria.key) {
            "bookType" ->
                build(
                    root.join<Book, Series>("series").join<Series, BookType>("bookType"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )
            else -> super.toPredicate(root, query, builder)
        }
    }
}
