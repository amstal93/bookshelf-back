package bzh.lautre.bookshelf.specification.builder

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import org.springframework.data.jpa.domain.Specification

class GenericSpecificationsBuilder<U> {
    private val params: MutableList<SpecSearchCriteria>

    fun with(
        key: String,
        operation: String,
        value: Any?,
        prefix: String?,
        suffix: String?
    ): GenericSpecificationsBuilder<U> {
        return with(null, key, operation, value, prefix, suffix)
    }

    fun with(
        precedenceIndicator: String?,
        key: String,
        operation: String,
        value: Any?,
        prefix: String?,
        suffix: String?
    ): GenericSpecificationsBuilder<U> {
        var op: SearchOperation? = SearchOperation.getSimpleOperation(operation[0])
        if (op != null && op === SearchOperation.EQUALITY) {
            val startWithAsterisk = prefix != null && prefix.contains(SearchOperation.ZERO_OR_MORE_REGEX)
            val endWithAsterisk = suffix != null && suffix.contains(SearchOperation.ZERO_OR_MORE_REGEX)
            op = when {
                startWithAsterisk && endWithAsterisk -> SearchOperation.CONTAINS
                startWithAsterisk -> SearchOperation.ENDS_WITH
                endWithAsterisk -> SearchOperation.STARTS_WITH
                else -> null
            }
        }
        params.add(SpecSearchCriteria(precedenceIndicator, key, op, value))
        return this
    }

    fun build(
        searchCriteriaList: MutableList<SpecSearchCriteria>,
        converter: (SpecSearchCriteria?) ->  Specification<U>?
    ): Specification<U> {
        val searchCriteriaMap = mutableMapOf<String, MutableList<SpecSearchCriteria>>()

        searchCriteriaList.forEach{
            if (searchCriteriaMap.containsKey(it.key)) {
                searchCriteriaMap[it.key!!]!!.add(it)
            } else {
                searchCriteriaMap[it.key!!] = listOf(it).toMutableList()
            }
        }

        var global: Specification<U>? = Specification.where(null)!!
        for (specSearchCriteriaList in searchCriteriaMap.values) {
            global = if (specSearchCriteriaList[0].orPredicate)
                Specification.where(global)!!.or(createSpecificationForAKey(specSearchCriteriaList, converter))!!
            else
                Specification.where(global)!!.and(createSpecificationForAKey(specSearchCriteriaList, converter))!!
        }

        return global!!
    }

    init {
        params = java.util.ArrayList()
    }

    private fun createSpecificationForAKey(
        specSearchCriteriaList: MutableList<SpecSearchCriteria>,
        converter: (SpecSearchCriteria?) ->  Specification<U>?
    ): Specification<U> {
        var result: Specification<U> = Specification.where(null)!!
        for (searchCriteria in specSearchCriteriaList) {
            result = if (searchCriteria.orPredicate)
                Specification.where(result)!!.or(converter(searchCriteria))!!
            else
                Specification.where(result)!!.and(converter(searchCriteria))!!
        }

        return result
    }
}
