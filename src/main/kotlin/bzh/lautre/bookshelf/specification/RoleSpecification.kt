package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Contract
import bzh.lautre.bookshelf.model.ContractId
import bzh.lautre.bookshelf.model.Role
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Expression
import javax.persistence.criteria.Root

class RoleSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Role>(criteria) {
    companion object {
        fun orderRoles(direction: Sort.Direction, sort: String): Specification<Role> {
            return Specification { root: Root<Role>, query: CriteriaQuery<*>, builder: CriteriaBuilder ->
                val orderBys = mutableListOf<Expression<Long>>()
                if (sort == "count") {
                    query.groupBy(listOf(root.get<Long>("id"), root.get("name")))
                    orderBys.add(builder.count(root.join<Role, Contract>("contracts").get<ContractId>("id").get<String>("bookIsbn")))
                }

                if (direction == Sort.Direction.ASC) {
                    query.orderBy(orderBys.map { builder.asc(it) })
                } else {
                    query.orderBy(orderBys.map { builder.desc(it) })
                }
                null
            }
        }
    }
}
