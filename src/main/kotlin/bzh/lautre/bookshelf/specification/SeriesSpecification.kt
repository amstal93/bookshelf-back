package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.model.Series
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

class SeriesSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Series>(criteria) {

    companion object {
        fun distinct(distinct: Boolean = true): Specification<Series> {
            return Specification { _, query: CriteriaQuery<*>, _ ->
                query.distinct(distinct)
                null
            }
        }
    }

    override fun toPredicate(root: Root<Series>, query: CriteriaQuery<*>, builder: CriteriaBuilder): Predicate? {
        return when (this.criteria.key) {
            "bookType" ->
                build(
                    root.join<Book, BookType>("bookType"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )
            "bookStatus" ->
                build(
                    root.join<Series, Book>("bookList"),
                    this.criteria.operation, "status", this.criteria.value, builder
                )
            "editor" ->
                build(
                    root.join<Series, Editor>("editor"),
                    this.criteria.operation, "name", this.criteria.value, builder
                )
            else -> super.toPredicate(root, query, builder)
        }
    }
}

