package bzh.lautre.bookshelf.specification

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Borrower

class BorrowerSpecification(criteria: SpecSearchCriteria) : CommonSpecification<Borrower>(criteria)

