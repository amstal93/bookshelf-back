package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookType
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import java.util.*

interface BookBusiness: EntityBusiness<Book, String> {

    val allBooks: List<Book>
    val count: Long
    val unreadBookCount: Long
    fun search(specs: Specification<Book>, page: Long = 0, size: Long = 5, direction: Sort.Direction, sort: BookSortField): Page<Book>

    fun getByBookType(name: String): List<Book>
    fun countByBookType(bookType: BookType): Long
    fun getLastAdded(page: Long = 0, size: Long = 5): Page<Book>
    fun getByIsbnAndArkId(isbn: String, arkId: String?): Optional<Book>
    fun getAllByIsbn(isbns: List<String>): List<Book>
    fun getByIsbn(isbn: String): Book
}
