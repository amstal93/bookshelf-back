package bzh.lautre.bookshelf.business

interface VersionService {

    fun getVersionInformation(): VersionInformation

    class VersionInformation(
        val projectVersion: String,
        val pipelineId: String,
        val jobId: String,
        val commitSha: String
    )
}
