package bzh.lautre.bookshelf.business.model

import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.Role

class BooksByRole(
    val role: Role,
    val books: List<Book>
)
