package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.BookType

interface BookTypeBusiness: EntityBusiness<BookType, Long> {

    val count: Long
    val allBookTypes: List<BookType>
    fun getByName(name: String): BookType
    fun getTop3BookTypesInBooksNumber(): List<BookType>
}
