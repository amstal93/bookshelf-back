package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.TaskBusiness
import bzh.lautre.bookshelf.exception.AlreadyCreatedTask
import bzh.lautre.bookshelf.model.Task
import bzh.lautre.bookshelf.model.TaskStatusEnum
import bzh.lautre.bookshelf.model.TaskTypeEnum
import bzh.lautre.bookshelf.repository.TaskRepository
import lombok.extern.slf4j.Slf4j
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import java.util.*

@Component
@Slf4j
class TaskBusinessImpl(
    val taskRepository: TaskRepository
) : TaskBusiness {
    override fun getAllTasksPerStatus(status: TaskStatusEnum): List<Task> {
        return taskRepository.findAllByStatusOrderByCreateDateDesc(status)
    }

    override fun delete(t: Task): Boolean {
        taskRepository.delete(t)
        return this.findById(t.id!!).isPresent
    }

    override fun findById(id: Long): Optional<Task> {
        return taskRepository.findById(id)
    }

    override fun cleanAddBookTask(isbn: String) {
        taskRepository.findByTypeIsAndExtraIs(TaskTypeEnum.ADD_BOOK, isbn)
            .ifPresent {
                taskRepository.setTaskStatusAsDone(it.id!!)
            }
    }

    override fun setTaskAsDone(id: Long): Boolean {
        return taskRepository.setTaskStatusAsDone(id) > 0
    }

    override fun getPageOfLastTaskPerStatusAndType(
        type: TaskTypeEnum,
        status: TaskStatusEnum,
        page: Int, size: Int
    ): Page<Task> {
        return taskRepository.findAllByStatusInAndTypeInOrderByCreateDateDesc(
            listOf(status), listOf(type), PageRequest.of(page, size)
        )
    }

    @Throws(AlreadyCreatedTask::class)
    override fun save(t: Task): Task {
        try {
            return taskRepository.save(t)
        } catch (ex: DataIntegrityViolationException) {
            throw AlreadyCreatedTask(
                if (t.type == TaskTypeEnum.ADD_BOOK) {
                    "L'isbn " + t.extra + " a déjà ètè ajouté"
                } else {
                    "La tache a déjà été créée"
                }
            )
        }
    }
}
