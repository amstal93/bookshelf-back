package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.business.BorrowerBusiness
import bzh.lautre.bookshelf.model.Borrower
import bzh.lautre.bookshelf.repository.BorrowerRepository
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class BorrowerBusinessImpl(
    private val repository: BorrowerRepository
) : BorrowerBusiness {

    override fun search(
        specs: Specification<Borrower>,
        page: Long,
        size: Long,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<Borrower> {
        return this.repository.findAll(specs, PageRequest.of(page.toInt(), size.toInt()))
    }

    override fun save(t: Borrower): Borrower {
        return if (t.id != null) {
            try {
                Optional.of(repository.save(t))
            } catch (e: DataIntegrityViolationException) {
                repository.findByName(t.name!!)
            }
        } else {
            repository.findByName(t.name!!)
        }
            .orElseGet { repository.save(t) }
    }

    override fun findById(id: Long): Optional<Borrower> {
        return repository.findById(id)
    }

    override fun delete(t: Borrower): Boolean {
        this.repository.delete(t)
        return this.findById(t.id!!).isPresent
    }
}