package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.EditorBusiness
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.repository.EditorRepository
import bzh.lautre.bookshelf.specification.EditorSpecification
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.stereotype.Component
import java.util.*

@Component
class EditorBusinessImpl(
    private val editorRepository: EditorRepository
) : EditorBusiness {

    override val allEditors: List<Editor>
        get() = editorRepository.findAll()

    override fun search(
        specs: Specification<Editor>,
        page: Long,
        size: Long,
        direction: Sort.Direction,
        allResults: Boolean
    ): Page<Editor> {
        return this.editorRepository.findAll(
            specs.and(EditorSpecification(SpecSearchCriteria()).orderBy(direction, "name")),
            if (allResults) {
                Pageable.unpaged()
            } else {
                PageRequest.of(page.toInt(), size.toInt())
            }
        )
    }

    override fun findByName(name: String): Optional<Editor> {
        return editorRepository.findByName(name)
    }

    override fun getByName(name: String): Editor {
        return findByName(name).orElseGet { Editor(name) }
    }

    override fun save(t: Editor): Editor {
        t.name = t.name.trim()
        return editorRepository.findByName(t.name.trim())
            .orElseGet { editorRepository.save(t) }
    }
}
