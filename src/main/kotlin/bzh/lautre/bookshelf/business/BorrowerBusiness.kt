package bzh.lautre.bookshelf.business

import bzh.lautre.bookshelf.model.Borrower
import org.springframework.stereotype.Component

@Component
interface BorrowerBusiness: EntityBusiness<Borrower, Long>