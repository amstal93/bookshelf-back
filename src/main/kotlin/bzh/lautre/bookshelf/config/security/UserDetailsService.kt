package bzh.lautre.bookshelf.config.security

import bzh.lautre.bookshelf.model.Account
import bzh.lautre.bookshelf.repository.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserDetailsService(
    @Autowired private val accountRepository: AccountRepository
)  {
    fun getUserGrantedAuthority(username: String): MutableSet<GrantedAuthority> {
        val userDb: Account = accountRepository.findById(username)
            .orElseThrow { throw UsernameNotFoundException(String.format("Account [%s] not found", username)) }

        return mutableSetOf(SimpleGrantedAuthority(userDb.role.name))
    }

}
