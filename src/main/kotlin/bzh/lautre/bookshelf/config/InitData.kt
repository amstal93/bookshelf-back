package bzh.lautre.bookshelf.config

import bzh.lautre.bookshelf.config.properties.BookshelfProperties
import bzh.lautre.bookshelf.model.Account
import bzh.lautre.bookshelf.model.AccountRoleEnum
import bzh.lautre.bookshelf.repository.AccountRepository
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component


@Component
class InitData(
    private val accountRepository: AccountRepository,
    private val bookshelfProperties: BookshelfProperties
) {

    @EventListener
    fun appReady(event: ApplicationReadyEvent) {
        if (accountRepository.countByRole(AccountRoleEnum.ROLE_ADMIN) == 0L) {
            accountRepository.save(
                Account.Builder()
                    .email(bookshelfProperties.users!!["admin"]?.email!!)
                    .role(AccountRoleEnum.ROLE_ADMIN)
                    .build()
            )
        }
    }
}
