package bzh.lautre.bookshelf.config.resources

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class SearchFileResourceHandler : WebMvcConfigurer {

    @Value("\${bookshelf.covers.search.path}")
    private val resourcesPath: String = "/covers"

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        registry
            .addResourceHandler("/search/covers/**")
            .addResourceLocations("file:$resourcesPath")
    }
}
