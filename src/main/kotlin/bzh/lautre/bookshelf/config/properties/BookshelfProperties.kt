package bzh.lautre.bookshelf.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component
import javax.validation.constraints.NotEmpty

@Component
@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "bookshelf")
class BookshelfProperties {

    var covers: CoverConfiguration? = null
    var oauth2: Oauth2? = null
    var users: Map<String, Users>? = null

    class Users {
        @NotEmpty
        var email: String? = null
    }

    class CoverConfiguration {
        @NotEmpty
        var path: String? = null
        @NotEmpty
        var searchPath: String? = null
    }

    class Oauth2 {
        @NotEmpty
        var clientId: String? = null
        var server: Oauth2Server? = null
    }

    class Oauth2Server {
        @NotEmpty
        var url: String? = null
        @NotEmpty
        var realm: String? = null
    }

}
