package bzh.lautre.bookshelf.model

import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import javax.persistence.*

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "book_type")
class BookType(
        @Column(unique = true) var name: String = "",
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(columnDefinition = "BIGINT") var id: Long? = null,
        @OneToMany(mappedBy = "bookType", cascade = [CascadeType.PERSIST], fetch = FetchType.LAZY) val seriesList: MutableList<Series> = emptyList<Series>().toMutableList()
)
