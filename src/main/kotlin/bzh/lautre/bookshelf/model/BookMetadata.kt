package bzh.lautre.bookshelf.model

import lombok.Data
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import javax.persistence.*

@Data
@Entity
@Table(name = "book_metadata")
class BookMetadata(
    @Id
    @Column(name = "book_isbn")
    var isbn: String? = null,
    @OneToOne
    @MapsId
    @JoinColumn(name = "book_isbn")
    var book: Book? = null,
    @Column(columnDefinition="Decimal(10,2)")
    var price: BigDecimal? = null,
    var priceCurrency: String? = null,
    var acquisitionDate: String? = null,
    var originalReleaseDate: String? = null,
    var pageCount: Long = 0,
    var offered: Boolean = false
): java.io.Serializable