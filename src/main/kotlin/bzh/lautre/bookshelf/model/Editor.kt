package bzh.lautre.bookshelf.model

import lombok.Data

import javax.persistence.*

@Data
@Entity
@Table(name = "editor")
class Editor(
    @Column(unique = true) var name: String = "",
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(columnDefinition = "BIGINT") var id: Long? = null,
    @OneToMany(mappedBy = "editor", cascade = [CascadeType.ALL], orphanRemoval = true) var series: Set<Series> = setOf()
)
