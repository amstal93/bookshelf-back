package bzh.lautre.bookshelf.model

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class ContractId(
    @Column(name = "role_id", nullable = false) var roleId: Long? = null,
    @Column(name = "book_isbn", nullable = false) var bookIsbn: String? = null
): Serializable
