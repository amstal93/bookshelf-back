package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "loan")
class Loan(
    @ManyToOne var book: Book = Book(),
    @ManyToOne var borrower: Borrower = Borrower(),
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @Column(nullable = false) var borrowDate: Date = Date(),
    @Column var returnDate: Date? = null
): Serializable
