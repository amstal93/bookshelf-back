package bzh.lautre.bookshelf.model

enum class TaskStatusEnum {
    TODO, DONE
}
