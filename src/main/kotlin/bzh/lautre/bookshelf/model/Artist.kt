package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "artist")
class Artist(
        @Column(unique = true) var name: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
        @ManyToMany(mappedBy = "artists", cascade = [CascadeType.ALL]) var contracts: List<Contract> = mutableListOf(),
        @OneToMany(mappedBy = "artists", cascade = [CascadeType.ALL], orphanRemoval = true) var webLinks: List<WebLinks> = mutableListOf()
)
