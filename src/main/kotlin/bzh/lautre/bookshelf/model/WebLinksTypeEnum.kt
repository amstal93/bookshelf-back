package bzh.lautre.bookshelf.model

enum class WebLinksTypeEnum {
    TWITTER,
    ART_STATION,
    FACEBOOK,
    INSTAGRAM,
    DEVIANT_ART,
    OTHER
}
