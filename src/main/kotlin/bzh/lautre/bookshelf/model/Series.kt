package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import java.io.Serializable
import java.util.*

import javax.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "series")
class Series(
    @Column var name: String = "",
    @ManyToOne var editor: Editor = Editor(),
    var displayName: String = "",
    @ManyToOne(fetch = FetchType.LAZY) var bookType: BookType = BookType(),
    var oneShot: Boolean = false,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
    @OneToMany(
        mappedBy = "series",
        cascade = [CascadeType.MERGE],
        orphanRemoval = true
    ) var bookList: MutableList<Book> = mutableListOf(),
    var lastReadBookDate: Date = Date()
) : Serializable
