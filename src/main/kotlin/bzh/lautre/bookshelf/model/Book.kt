package bzh.lautre.bookshelf.model

import lombok.Data
import java.time.LocalDate
import java.util.*

import javax.persistence.*

@Data
@Entity
@Table(name = "book")
class Book (
        @Id var isbn: String? = null,
        var arkId: String? = null,
        var title: String? = null,
        var tome: Int? = null,
        var year: String? = null,
        var collection: String? = null,
        var cover: String? = null,
        var creationDate: LocalDate = LocalDate.now(),
        var lastUpdateDate: LocalDate = LocalDate.now(),
        @Enumerated(EnumType.STRING) @Column(length = 7)
        var status: BookReadStatusEnum = BookReadStatusEnum.UNREAD,
        @OneToMany(fetch = FetchType.EAGER, mappedBy = "book", cascade = [CascadeType.ALL],  orphanRemoval = true)
        var contracts: List<Contract> = mutableListOf(),
        @ManyToOne(fetch = FetchType.LAZY)
        var series: Series? = null,
        @OneToMany(mappedBy = "book", cascade = [CascadeType.ALL])
        var loans: List<Loan> = mutableListOf()
) {
        @OneToOne
        var currentLoan: Loan? = null
        @OneToOne(mappedBy = "book", cascade = [CascadeType.ALL])
        @PrimaryKeyJoinColumn
        var metadata: BookMetadata? = null
}
