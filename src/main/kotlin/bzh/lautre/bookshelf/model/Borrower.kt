package bzh.lautre.bookshelf.model

import lombok.Data
import lombok.NoArgsConstructor
import javax.persistence.*

@Data
@NoArgsConstructor
@Entity
@Table(name = "borrower")
class Borrower(
        @Column(unique = true) var name: String? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null,
        @OneToMany(mappedBy = "borrower", cascade = [CascadeType.ALL]) var loans: MutableList<Loan> = mutableListOf()
)
