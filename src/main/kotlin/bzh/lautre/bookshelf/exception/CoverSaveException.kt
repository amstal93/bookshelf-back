package bzh.lautre.bookshelf.exception

class CoverSaveException(message: String) : CustomInternalErrorException(message, "COVER_SAVE_EXCEPTION")
