package bzh.lautre.bookshelf.exception

class AlreadyCreatedTask(message: String) : CustomInternalErrorException(message, "ALREADY_CREATED")
