package bzh.lautre.bookshelf.exception

class ImpossibleSeriesSave(message: String) : CustomInternalErrorException(message, "IMPOSSIBLE_TO_SAVE_SERIES")
