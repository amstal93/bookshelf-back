package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import bzh.lautre.bookshelf.model.BookType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface BookRepository : JpaRepository<Book, String>, JpaSpecificationExecutor<Book> {
    fun findByIsbnAndArkId(isbn: String?, arkId: String?): Optional<Book>
    fun findAllBySeries_BookType_Name(name: String): List<Book>
    fun countBooksBySeries_BookType(bookType: BookType): Long
    fun countAllByStatusNotIn(statusList: List<BookReadStatusEnum>): Long
}
