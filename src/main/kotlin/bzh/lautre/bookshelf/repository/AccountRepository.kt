package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Account
import bzh.lautre.bookshelf.model.AccountRoleEnum
import org.springframework.data.jpa.repository.JpaRepository

interface AccountRepository : JpaRepository<Account, String> {
    fun countByRole(accountRoleEnum: AccountRoleEnum): Long
}
