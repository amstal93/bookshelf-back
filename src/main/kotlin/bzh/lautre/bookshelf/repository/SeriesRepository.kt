package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.BookType
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.model.Series
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface SeriesRepository : JpaRepository<Series, Long>, JpaSpecificationExecutor<Series> {
    fun findByNameAndEditorAndBookType(name: String, editor: Editor, bookType: BookType): Optional<Series>
}
