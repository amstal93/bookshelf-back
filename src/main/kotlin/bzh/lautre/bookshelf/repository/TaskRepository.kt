package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Task
import bzh.lautre.bookshelf.model.TaskStatusEnum
import bzh.lautre.bookshelf.model.TaskTypeEnum
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import java.util.*
import javax.transaction.Transactional

@Transactional
interface TaskRepository : JpaRepository<Task, Long> {
    fun findAllByStatusOrderByCreateDateDesc(status: TaskStatusEnum): List<Task>

    fun findAllByStatusInAndTypeInOrderByCreateDateDesc(
        status: Collection<TaskStatusEnum>,
        type: Collection<TaskTypeEnum>,
        pageable: Pageable
    ): Page<Task>

    fun findByTypeIsAndExtraIs(type: TaskTypeEnum, extra: String): Optional<Task>

    @Modifying
    @Query("update Task t set t.status = 'DONE' where t.id = :id")
    fun setTaskStatusAsDone(id: Long): Int
}
