package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.BookType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface BookTypeRepository : JpaRepository<BookType, Long>, JpaSpecificationExecutor<BookType> {
    fun getBookTypeByName(name: String): Optional<BookType>
}
