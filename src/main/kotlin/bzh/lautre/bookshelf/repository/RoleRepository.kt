package bzh.lautre.bookshelf.repository

import bzh.lautre.bookshelf.model.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import java.util.*

interface RoleRepository : JpaRepository<Role, Long>, JpaSpecificationExecutor<Role> {

    fun findByName(name: String): Optional<Role>

}
