package bzh.lautre.bookshelf.api.config

import bzh.lautre.bookshelf.config.properties.BookshelfProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.OAuthBuilder
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger.web.SecurityConfiguration
import springfox.documentation.swagger.web.SecurityConfigurationBuilder
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig(
    internal val swaggerDocumentationConfig: SwaggerDocumentationConfig,
    private val bookshelfProperties: BookshelfProperties
) {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .groupName("Generated")
            .select()
            .apis(RequestHandlerSelectors.basePackage("bzh.lautre.bookshelf"))
            .build()
            .securitySchemes(listOf(oAuthSecuritySchema()))
            .securityContexts(listOf(securityContext()))
            .pathMapping("/")
            .directModelSubstitute(java.time.LocalDate::class.java, java.sql.Date::class.java)
            .directModelSubstitute(java.time.OffsetDateTime::class.java, java.util.Date::class.java)
            .apiInfo(apiInfo())
    }

    private fun securityContext(): SecurityContext {
        return SecurityContext.builder().securityReferences(listOf(SecurityReference("oauth2schema", emptyArray()))).build()
    }

    private fun oAuthSecuritySchema(): OAuth {
        val url = "${bookshelfProperties.oauth2?.server?.url}/${bookshelfProperties.oauth2?.server?.realm}"

        return OAuthBuilder()
            .name("oauth2schema")
            .grantTypes(
                listOf(
                    AuthorizationCodeGrant(
                        TokenRequestEndpoint("${url}/protocol/openid-connect/auth", bookshelfProperties.oauth2?.clientId, ""),
                        TokenEndpoint("${url}/protocol/openid-connect/token", "")
                    )
                )
            )
            .build()
    }

    @Bean
    fun securityInfo(): SecurityConfiguration {
        return SecurityConfigurationBuilder.builder()
            .clientId(bookshelfProperties.oauth2?.clientId)
            .clientSecret("")
            .realm("bookshelf")
            .appName("bookshelf-back")
            .scopeSeparator(" ")
            .build()
    }

    private fun apiInfo(): ApiInfo {
        return this.swaggerDocumentationConfig.apiInfo()
    }
}
