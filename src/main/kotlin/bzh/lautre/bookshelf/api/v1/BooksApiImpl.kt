package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.ArtistMapper
import bzh.lautre.bookshelf.api.v1.mapper.BookMapper
import bzh.lautre.bookshelf.api.v1.mapper.RoleMapper
import bzh.lautre.bookshelf.api.v1.mapper.SeriesMapper
import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.*
import bzh.lautre.bookshelf.business.model.BookSortField
import bzh.lautre.bookshelf.exception.CoverSaveException
import bzh.lautre.bookshelf.exception.ResourceNotFound
import bzh.lautre.bookshelf.model.Artist
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.Contract
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.specification.BookSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import java.time.LocalDate
import java.util.*
import java.util.stream.Collectors
import javax.validation.Valid

@RestController
@Api(tags = ["book"])
class BooksApiImpl @Autowired
constructor(
    private val bookBusiness: BookBusiness,
    private val artistBusiness: ArtistBusiness,
    private val roleBusiness: RoleBusiness,
    private val seriesBusiness: SeriesBusiness,
    private val coverBusiness: CoverBusiness,
    private val bookMapper: BookMapper,
    private val artistMapper: ArtistMapper,
    private val roleMapper: RoleMapper,
    private val seriesMapper: SeriesMapper,
    private val contractBusiness: ContractBusiness
) : BooksApi {

    override fun searchBooks(
        search: String?,
        page: Long?,
        size: Long?,
        direction: String?,
        sort: String?
    ): ResponseEntity<MutableList<MinimalBookDTO>> {
        val itemPage: Page<Book> =
            this.bookBusiness.search(
                resolveSpecificationFromInfixExpr(search ?: ""),
                page!!,
                size!!,
                Sort.Direction.fromString(direction!!),
                BookSortField.valueOf(sort!!.uppercase())
            )

        return ResponseEntity(
            itemPage.stream().map(bookMapper::mapToMinimal)
                .collect(Collectors.toCollection { LinkedList<MinimalBookDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/books"),
            HttpStatus.OK
        )
    }

    override fun saveBook(@Valid bookDTO: BookDTO): ResponseEntity<BookDTO> {
        if (bookDTO.series == null || bookDTO.editor == null) {
            return ResponseEntity.badRequest().build()
        }

        // Sanitize
        bookDTO.isbn = bookDTO.isbn!!
            .replace("-", "")
            .replace(" ", "")
            .trim { it <= ' ' }

        this.bookBusiness.getByIsbnAndArkId(bookDTO.isbn!!, bookDTO.arkId).ifPresent {
            ResponseEntity
                .status(HttpStatus.CONFLICT)
                .location(getLocationURI(it))
        }

        val createdBook = createBook(
            this.bookMapper.map(bookDTO),
            seriesMapper.map(bookDTO.series)
        )

        createdBook.contracts = bookDTO.contracts.map {
            contractBusiness.save(createContract(it, createdBook))
        }

        return ResponseEntity
            .created(getLocationURI(createdBook))
            .body(bookMapper.map(createdBook))

    }

    private fun createBook(book: Book, series: Series, ): Book {
        book.metadata?.book = book

        val dbSeries: Series = seriesBusiness.save(series)
        series.bookList.add(book)
        book.series = dbSeries

        return this.bookBusiness.save(book)
    }

    private fun createContract(contractDto: ContractDTO, createdBook: Book): Contract {
        return Contract(
            roleBusiness.save(roleMapper.map(contractDto.role)),
            contractDto.artists.map { artistBusiness.save(mapArtist(it)) },
            createdBook
        )
    }

    private fun mapArtist(artist: MinimalArtistDTO): Artist {
        return artistMapper.map(artist)
    }

    private fun getLocationURI(book: Book): URI {
        return ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand(book.isbn)
            .toUri()
    }

    override fun deleteBook(isbn: String?): ResponseEntity<Void> {
        val book = this.bookBusiness.findById(isbn!!)
            .orElseThrow { ResourceNotFound("The resource doesn't exists", "BOOK", isbn) }
        this.bookBusiness.delete(book)
        return if (this.bookBusiness.findById(book.isbn!!).isPresent) {
            throw InternalError("The resource book $isbn hasn't been removed")
        } else {
            ResponseEntity.noContent().build()
        }
    }

    override fun updateBook(isbn: String?, bookDTO: BookDTO): ResponseEntity<BookDTO> {
        if (isbn != bookDTO.isbn) {
            return ResponseEntity.badRequest().build()
        }

        if (this.bookBusiness.findById(isbn!!).isPresent) {
            return ResponseEntity
                .ok()
                .body(update(this.bookMapper.map(bookDTO), bookDTO))
        }

        throw ResourceNotFound("The resource doesn't exists", "BOOK", isbn)
    }

    override fun patchBook(isbn: String?, bookDTO: BookDTO): ResponseEntity<BookDTO> {
        if (bookDTO.isbn != null && isbn != bookDTO.isbn) {
            return ResponseEntity.badRequest().build()
        }

        val optional: Optional<Book> = this.bookBusiness.findById(isbn!!)
        if (optional.isPresent) {
            return ResponseEntity
                .ok()
                .body(update(this.bookMapper.map(bookDTO, optional.get()), bookDTO))
        }

        throw ResourceNotFound("The resource doesn't exists", "BOOK", isbn)
    }

    private fun update(book: Book, bookDTO: BookDTO): BookDTO {
        book.metadata?.book = book

        val series: Series = seriesBusiness.save(book.series!!)
        series.bookList.add(book)
        book.series = series
        book.lastUpdateDate = LocalDate.now()
        if (bookDTO.contracts !== null) {
            book.contracts = bookDTO.contracts.map {
                contractBusiness.save(createContract(it, book))
            }
        }
        return this.bookMapper.map(this.bookBusiness.save(book))
    }

    override fun getBookByIsbn(isbn: String): ResponseEntity<BookDTO> {
        return ResponseEntity.ok(this.bookBusiness.findById(isbn).map { bookMapper.map(it) }
            .orElseThrow { ResourceNotFound("The resource doesn't exists", "BOOK", isbn) })
    }

    override fun putBooksSearchCover(isbn: String?, file: MultipartFile?): ResponseEntity<InlineResponse200DTO> {
        return if (this.coverBusiness.uploadCover(file!!, isbn!!)) {
            ResponseEntity.ok(InlineResponse200DTO().cover("$isbn.jpg"))
        } else {
            throw CoverSaveException("Error during the save of $isbn cover")
        }
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Book> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Book> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { BookSpecification(it!!) }
    }
}

