package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.ArtistDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalArtistDTO
import bzh.lautre.bookshelf.model.Artist
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(
    componentModel = "spring",
    uses = [WebLinksMapper::class]
)
interface ArtistMapper {

    fun map(artist: Artist?): ArtistDTO

    @InheritInverseConfiguration
    @Mapping(target = "contracts", ignore = true)
    fun map(artist: ArtistDTO): Artist

    fun map(artist: MinimalArtistDTO): Artist

}
