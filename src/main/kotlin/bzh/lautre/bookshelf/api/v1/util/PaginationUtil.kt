package bzh.lautre.bookshelf.api.v1.util

import org.springframework.data.domain.Page
import org.springframework.http.HttpHeaders
import org.springframework.web.util.UriComponentsBuilder
import java.net.URISyntaxException

class PaginationUtil private constructor() {
    companion object {
        @Throws(URISyntaxException::class)
        fun generatePaginationHttpHeaders(page: Page<*>, baseUrl: String): HttpHeaders {
            val headers = HttpHeaders()
            headers.add("X-Total-Count", page.totalElements.toString())
            headers.add("X-Page-Size", page.size.toString())
            headers.add("X-Total-Page", page.totalPages.toString())
            headers.add("X-Page-Number", page.number.toString())
            var link = ""
            if (page.number + 1 < page.totalPages) {
                link = "<" + generateUri(baseUrl, page.number + 1, page.size) + ">; rel=\"next\","
            }
            // prev link
            if (page.number > 0) {
                link += "<" + generateUri(baseUrl, page.number - 1, page.size) + ">; rel=\"prev\","
            }
            // last and first link
            var lastPage = 0
            if (page.totalPages > 0) {
                lastPage = page.totalPages - 1
            }
            link += "<" + generateUri(baseUrl, lastPage, page.size) + ">; rel=\"last\","
            link += "<" + generateUri(baseUrl, 0, page.size) + ">; rel=\"first\""
            headers.add(HttpHeaders.LINK, link)
            return headers
        }

        @Throws(URISyntaxException::class)
        private fun generateUri(baseUrl: String, page: Int, size: Int): String {
            var uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl)
            uriComponentsBuilder = if (baseUrl.contains("page=")) {
                uriComponentsBuilder.replaceQueryParam("page", page)
            } else {
                uriComponentsBuilder.queryParam("page", page)
            }
            uriComponentsBuilder = if (baseUrl.contains("size=")) {
                uriComponentsBuilder.replaceQueryParam("size", size)
            } else {
                uriComponentsBuilder.queryParam("size", size)
            }
            return uriComponentsBuilder.toUriString()
        }
    }

    init {
        throw UnsupportedOperationException()
    }
}
