package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.BookTypeDTO
import bzh.lautre.bookshelf.model.BookType
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping


@Mapper(componentModel = "spring")
interface BookTypeMapper {

    @Mapping(target = "nbSeries", expression = "java(bookType.getSeriesList().size())")
    fun map(bookType: BookType): BookTypeDTO

    @InheritInverseConfiguration
    @Mapping(target = "seriesList", ignore = true)
    fun map(bookType: BookTypeDTO): BookType

}
