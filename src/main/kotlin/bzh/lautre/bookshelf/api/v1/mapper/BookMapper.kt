package bzh.lautre.bookshelf.api.v1.mapper

import bzh.lautre.bookshelf.api.v1.model.BookDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookDTO
import bzh.lautre.bookshelf.api.v1.model.MinimalBookWithCoverDTO
import bzh.lautre.bookshelf.api.v1.model.SeriesDTO
import bzh.lautre.bookshelf.business.BookTypeBusiness
import bzh.lautre.bookshelf.model.Book
import bzh.lautre.bookshelf.model.BookReadStatusEnum
import org.mapstruct.*
import org.springframework.stereotype.Component


@Mapper(
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ON_IMPLICIT_CONVERSION,
    componentModel = "spring",
    uses = [BookTypeBusiness::class, SeriesMapper::class, RoleMapper::class, ArtistMapper::class, EditorMapper::class],
    injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
@Component
abstract class BookMapper {

    @Mapping(target = "editor", source = "series.editor")
    abstract fun map(book: Book): BookDTO

    abstract fun map(books: List<Book>): List<BookDTO>

    @Mapping(target = "contracts", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    abstract fun map(book: BookDTO): Book

    @AfterMapping
    fun afterMapping(bookDto: BookDTO, @MappingTarget book: Book) {
        book.metadata?.isbn = book.isbn
    }


    @Condition
    fun isNotEmpty(value: String?): Boolean {
        return value != null && value.isNotEmpty()
    }

    @Condition
    fun isNotNull(value: Number?): Boolean {
        return value != null
    }

    @Condition
    fun isNotNull(value: SeriesDTO?): Boolean {
        return value != null
    }

    @Condition
    fun isNotEmpty(value: BookReadStatusEnum?): Boolean {
        return value != null
    }

    @Mapping(target = "contracts", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    abstract fun map(bookDTO: BookDTO, @MappingTarget book: Book): Book

    @Mapping(target = "editor", source = "series.editor")
    abstract fun mapToMinimal(book: Book): MinimalBookDTO

    abstract fun mapToMinimal(books: List<Book>): List<MinimalBookDTO>

    @Mapping(target = "editor", source = "series.editor")
    abstract fun mapToMinimalWithCover(book: Book): MinimalBookWithCoverDTO

    abstract fun mapToMinimalWithCover(books: List<Book>): List<MinimalBookWithCoverDTO>
}
