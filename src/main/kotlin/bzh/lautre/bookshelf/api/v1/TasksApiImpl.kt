package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.TaskMapper
import bzh.lautre.bookshelf.api.v1.model.*
import bzh.lautre.bookshelf.business.TaskBusiness
import bzh.lautre.bookshelf.model.TaskStatusEnum
import bzh.lautre.bookshelf.model.TaskTypeEnum
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal

@RestController
@Api(tags = ["task"])
class TasksApiImpl(
    private val business: TaskBusiness,
    private val mapper: TaskMapper
) : TasksApi {

    override fun addTask(taskDTO: TaskDTO?): ResponseEntity<TaskDTO> {
        return ok(mapper.map(business.save(mapper.map(taskDTO!!))))
    }

    override fun getTasksCountPerStatus(status: TaskStatusEnumDTO): ResponseEntity<TaskCountDTO> {
        val taskCountDTO = TaskCountDTO()
        val todoList = this.business.getAllTasksPerStatus(TaskStatusEnum.valueOf(status.name))
        taskCountDTO["ALL"] = BigDecimal(todoList.size)
        todoList.forEach {
            if (taskCountDTO.keys.contains(it.type.name)) {
                taskCountDTO[it.type.name] = taskCountDTO[it.type.name]?.plus(BigDecimal.ONE)
            } else {
                taskCountDTO[it.type.name] = BigDecimal.ONE
            }
        }
        return ok(taskCountDTO)
    }

    override fun deleteTask(id: Long?): ResponseEntity<Void> {
        var response: ResponseEntity<Void> = ResponseEntity.notFound().build()
        business.findById(id!!).ifPresent {
            business.delete(it)
            response = ResponseEntity.status(HttpStatus.NO_CONTENT).build()
        }
        return response
    }

    override fun getTasks(
        type: TaskTypeEnumDTO,
        status: TaskStatusEnumDTO,
        page: Long?,
        size: Long?
    ): ResponseEntity<PageOfTaskDTO> {
        val pageOfTask = business.getPageOfLastTaskPerStatusAndType(
            TaskTypeEnum.valueOf(type.name),
            TaskStatusEnum.valueOf(status.name),
            page!!.toInt(),
            size!!.toInt()
        )

        return ok(
            PageOfTaskDTO()
                .list(mapper.toDto(pageOfTask.content))
                .currentPage(pageOfTask.pageable.pageNumber.toLong())
                .totalElements(pageOfTask.totalElements)
                .totalPages(pageOfTask.totalPages.toLong())
        )
    }
}

