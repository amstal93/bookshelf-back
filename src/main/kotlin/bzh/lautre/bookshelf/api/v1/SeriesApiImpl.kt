package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.SeriesMapper
import bzh.lautre.bookshelf.api.v1.model.SeriesDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.SeriesBusiness
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.specification.SeriesSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.annotations.Api
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
@Api(tags = ["series"])
class SeriesApiImpl(
    private val business: SeriesBusiness,
    private val mapper: SeriesMapper
) : SeriesApi {

    override fun searchSeries(
        search: String?,
        page: Long?,
        size: Long?,
        direction: String?,
        allResults: Boolean?
    ): ResponseEntity<MutableList<SeriesDTO>> {
       return this.search(search, page, size, direction, allResults)
    }

    override fun searchSeriesAutocomplete(
        search: String?
    ): ResponseEntity<MutableList<SeriesDTO>> {
       return this.search(search, 0, 5, "DESC", false)
    }

    private fun search(
        search: String?,
        page: Long?,
        size: Long?,
        direction: String?,
        allResults: Boolean?
    ): ResponseEntity<MutableList<SeriesDTO>> {
        val itemPage: Page<Series> =
            this.business.search(
                resolveSpecificationFromInfixExpr(search ?: ""),
                page!!,
                size!!,
                Sort.Direction.fromString(direction!!),
                allResults!!
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList<SeriesDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/series"),
            HttpStatus.OK
        )
    }

    override fun updateSeries(id: Long?, series: SeriesDTO): ResponseEntity<SeriesDTO> {
        if (id != series.id) {
            return ResponseEntity.badRequest().build()
        }

        return ResponseEntity.ok(mapper.map(this.business.save(this.mapper.map(series))))
    }

    override fun deleteSeries(id: Long?): ResponseEntity<Void> {
        val optionalBookType = this.business.findById(id!!)
        return if (optionalBookType.isPresent) {
            if (this.business.delete(optionalBookType.get()))
                ResponseEntity.status(500).build()
            else
                ResponseEntity.noContent().build()
        } else {
            ResponseEntity.notFound().build()
        }
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Series> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Series> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { SeriesSpecification(it!!) }
    }
}

