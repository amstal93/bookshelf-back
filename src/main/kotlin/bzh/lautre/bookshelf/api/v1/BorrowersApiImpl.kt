package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.BorrowerMapper
import bzh.lautre.bookshelf.api.v1.mapper.LoanMapper
import bzh.lautre.bookshelf.api.v1.model.BorrowerDTO
import bzh.lautre.bookshelf.api.v1.model.LoanDTO
import bzh.lautre.bookshelf.api.v1.model.LoanWithBookDetailsDTO
import bzh.lautre.bookshelf.api.v1.model.SeriesDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.BorrowerBusiness
import bzh.lautre.bookshelf.business.LoanBusiness
import bzh.lautre.bookshelf.model.Borrower
import bzh.lautre.bookshelf.model.Loan
import bzh.lautre.bookshelf.model.Series
import bzh.lautre.bookshelf.specification.BorrowerSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.annotations.Api
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
@Api(tags = ["borrower"])
class BorrowersApiImpl(
    private val business: BorrowerBusiness,
    private val loanBusiness: LoanBusiness,
    private val bookBusiness: BookBusiness,
    private val mapper: BorrowerMapper,
    private val loanMapper: LoanMapper
) : BorrowersApi {

    override fun searchBorrowers(
        search: String?,
        page: Long?,
        size: Long?,
        direction: String?
    ): ResponseEntity<MutableList<BorrowerDTO>> {
        return this.search(search, page, size, direction)
    }

    override fun searchBorrowersAutocomplete(search: String?): ResponseEntity<MutableList<BorrowerDTO>> {
        return this.search(search, 0, 5, "DESC")
    }

    private fun search(
        search: String?,
        page: Long?,
        size: Long?,
        direction: String?
    ): ResponseEntity<MutableList<BorrowerDTO>> {
        val itemPage: Page<Borrower> =
            this.business.search(
                resolveSpecificationFromInfixExpr(search ?: ""),
                page!!,
                size!!,
                Sort.Direction.fromString(direction!!)
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/borrowers"),
            HttpStatus.OK
        )
    }

    override fun createBorrower(body: BorrowerDTO?): ResponseEntity<BorrowerDTO> {
        return ok(mapper.map(business.save(mapper.map(body!!))))
    }

    override fun getBorrowerById(id: Long?): ResponseEntity<BorrowerDTO> {
        return ResponseEntity.of(this.business.findById(id!!).map { mapper.map(it) })
    }

    override fun updateBorrower(id: Long?, borrower: BorrowerDTO): ResponseEntity<BorrowerDTO> {
        return if (id == borrower.id.toLong()) {
            ok().body(this.mapper.map(this.business.save(this.mapper.map(borrower))))
        } else {
            ResponseEntity.notFound().build()
        }
    }

    override fun deleteBorrower(id: Long?): ResponseEntity<Void> {
        var response: ResponseEntity<Void> = ResponseEntity.notFound().build()
        business.findById(id!!).ifPresent {
            business.delete(it)
            response = ResponseEntity.status(HttpStatus.NO_CONTENT).build()
        }
        return response
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Borrower> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Borrower> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { BorrowerSpecification(it!!) }
    }

    override fun createLoan(id: Long?, body: LoanDTO?): ResponseEntity<LoanDTO> {
        val borrowerOptional = this.business.findById(id!!)
        if (borrowerOptional.isEmpty) {
            return ResponseEntity.notFound().build()
        } else if (body?.bookIsbn == null) {
            return ResponseEntity.badRequest().build()
        }

        val book = this.bookBusiness.getByIsbn(body.bookIsbn)

        return ok(loanMapper.map(loanBusiness.save(Loan(book, borrowerOptional.get()))))
    }

    override fun updateLoan(id: Long?, subId: Long?, body: LoanDTO?): ResponseEntity<LoanWithBookDetailsDTO> {
        val borrowerOptional = this.business.findById(id!!)

        if (borrowerOptional.isEmpty) {
            return ResponseEntity.notFound().build()
        }

        if (body === null || body.bookIsbn === null) {
            return ResponseEntity.badRequest().build()
        }

        val loan = this.loanMapper.mapToEntity(body)
        val bookOptional = this.bookBusiness.findById(body.bookIsbn)
        if (bookOptional.isEmpty) {
            return ResponseEntity.badRequest().build()
        }
        loan.book = bookOptional.get()
        loan.borrower = borrowerOptional.get()
        return ok().body(this.loanMapper.mapWithBookDetails(this.loanBusiness.save(loan)))
    }

    override fun getBorrowersLoansById(id: Long?): ResponseEntity<MutableList<LoanWithBookDetailsDTO>> {
        val borrowerOptional = this.business.findById(id!!)
        if (borrowerOptional.isEmpty) {
            return ResponseEntity.notFound().build()
        }

        return ok(loanMapper.mapWithBookDetails(borrowerOptional.get().loans))
    }

    override fun deleteLoan(id: Long?, subId: Long?): ResponseEntity<Void> {
        if (this.business.findById(id!!).isEmpty) {
            return ResponseEntity.notFound().build()
        }

        val optional = this.loanBusiness.findById(subId!!)
        return if (optional.isPresent) {
            this.loanBusiness.delete(optional.get())
            if (this.loanBusiness.findById(subId).isPresent)
                ResponseEntity.status(500).build()
            else
                ResponseEntity.noContent().build()
        } else {
            ResponseEntity.notFound().build()
        }
    }
}

