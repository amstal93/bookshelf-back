package bzh.lautre.bookshelf.api.v1

import bzh.lautre.bookshelf.api.v1.mapper.RoleMapper
import bzh.lautre.bookshelf.api.v1.model.RoleDTO
import bzh.lautre.bookshelf.api.v1.util.CriteriaParser
import bzh.lautre.bookshelf.api.v1.util.PaginationUtil
import bzh.lautre.bookshelf.business.RoleBusiness
import bzh.lautre.bookshelf.model.Role
import bzh.lautre.bookshelf.specification.RoleSpecification
import bzh.lautre.bookshelf.specification.builder.GenericSpecificationsBuilder
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import java.util.*
import java.util.stream.Collectors

@RestController
@Api(tags = ["role"])
class RoleApiImpl @Autowired
constructor(
    private val business: RoleBusiness,
    private val mapper: RoleMapper
) : RolesApi {

    override fun searchRoles(
        search: String?,
        page: Long?,
        size: Long?,
        direction: String?
    ): ResponseEntity<MutableList<RoleDTO>> {
        return search(search, page, size, direction)
    }

    override fun searchRolesAutocomplete(
        search: String?,
        sort: String?
    ): ResponseEntity<MutableList<RoleDTO>> {
        return search(search, 0, 5, "DESC", sort!!)
    }

    private fun search(
        search: String?,
        page: Long?,
        size: Long?,
        direction: String?,
        sort: String = ""
    ): ResponseEntity<MutableList<RoleDTO>> {
        val itemPage: Page<Role> =
            this.business.search(
                resolveSpecificationFromInfixExpr(search ?: ""),
                page!!,
                size!!,
                Sort.Direction.fromString(direction!!),
                sort.lowercase()
            )

        return ResponseEntity(
            itemPage.stream().map(mapper::map).collect(Collectors.toCollection { LinkedList<RoleDTO>() }),
            PaginationUtil.generatePaginationHttpHeaders(itemPage, "/roles"),
            HttpStatus.OK
        )
    }

    protected fun resolveSpecificationFromInfixExpr(searchParameters: String): Specification<Role> {
        val parser = CriteriaParser()
        val specBuilder: GenericSpecificationsBuilder<Role> = GenericSpecificationsBuilder()
        return specBuilder.build(parser.parse(searchParameters)) { RoleSpecification(it!!) }
    }
}

