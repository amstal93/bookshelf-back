package bzh.lautre.bookshelf.api.v1.util

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CriteriaParserTest {

    @Test
    fun parseSimpleEquality() {
        val output = CriteriaParser().parse("toto:test")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.EQUALITY, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseSimpleContains() {
        val output = CriteriaParser().parse("toto:*test*")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.CONTAINS, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseSimpleEndsWith() {
        val output = CriteriaParser().parse("toto:*test")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.ENDS_WITH, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseSimpleStartsWith() {
        val output = CriteriaParser().parse("toto:test*")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.STARTS_WITH, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseSimpleGreaterThan() {
        val output = CriteriaParser().parse("toto>test")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.GREATER_THAN, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseSimpleLessThan() {
        val output = CriteriaParser().parse("toto<test")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.LESS_THAN, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseSimpleLike() {
        val output = CriteriaParser().parse("toto~test")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.LIKE, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseSimpleNegation() {
        val output = CriteriaParser().parse("toto!test")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test", output[0].value)
        assertEquals(SearchOperation.NEGATION, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseEndsWithOrStartsWith() {
        val output = CriteriaParser().parse("toto:*test, test|'tata:titi*")
        assertEquals(2, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test, test", output[0].value)
        assertEquals(SearchOperation.ENDS_WITH, output[0].operation)
        assertFalse(output[0].orPredicate)
        assertEquals("tata", output[1].key)
        assertEquals("titi", output[1].value)
        assertEquals(SearchOperation.STARTS_WITH, output[1].operation)
        assertTrue(output[1].orPredicate)
    }

    @Test
    fun parseSimpleIn() {
        val output = CriteriaParser().parse("toto:[test,titi]")
        assertEquals(1, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test,titi", output[0].value)
        assertEquals(SearchOperation.IN, output[0].operation)
        assertFalse(output[0].orPredicate)
    }

    @Test
    fun parseAnd() {
        val output = CriteriaParser().parse("toto:[test,titi]|tata>titi*")
        assertEquals(2, output.size)
        assertEquals("toto", output[0].key)
        assertEquals("test,titi", output[0].value)
        assertEquals(SearchOperation.IN, output[0].operation)
        assertFalse(output[0].orPredicate)
        assertEquals("tata", output[1].key)
        assertEquals("titi", output[1].value)
        assertEquals(SearchOperation.GREATER_THAN, output[1].operation)
        assertFalse(output[1].orPredicate)
    }
}
