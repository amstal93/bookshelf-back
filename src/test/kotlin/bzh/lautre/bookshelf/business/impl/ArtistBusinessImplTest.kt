package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.business.BookBusiness
import bzh.lautre.bookshelf.business.RoleBusiness
import bzh.lautre.bookshelf.business.model.BooksByRole
import bzh.lautre.bookshelf.model.*
import bzh.lautre.bookshelf.repository.ArtistRepository
import bzh.lautre.bookshelf.specification.ArtistSpecification

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.inOrder
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import java.util.*


@ExtendWith(MockitoExtension::class)
internal class ArtistBusinessImplTest {
    @Mock
    lateinit var mockArtistRepository: ArtistRepository

    @Mock
    lateinit var mockRoleBusiness: RoleBusiness

    @Mock
    lateinit var mockBookBusiness: BookBusiness

    @InjectMocks
    lateinit var artistBusiness: ArtistBusinessImpl

    @Test
    fun search() {
        val artist = Artist("name", 1)
        val page = PageImpl(listOf(artist))
        val specification = ArtistSpecification(SpecSearchCriteria())

        /* Given */
        Mockito.`when`(mockArtistRepository.findAll(specification, PageRequest.of(0, 1))).thenReturn(page)

        /* When */
        assertEquals(page, artistBusiness.search(specification, 0, 1))

        /* Then */
        verify(mockArtistRepository).findAll(specification, PageRequest.of(0, 1))
    }

    @Test
    fun `save without id name not present`(){
        val artist = Artist("name", null)
        artist.webLinks = mutableListOf()

        val savedArtist = Artist("name", 1)
        savedArtist.webLinks = mutableListOf()

        Mockito.`when`(mockArtistRepository.findByName(artist.name!!)).thenReturn(Optional.empty())
        Mockito.`when`(mockArtistRepository.save(artist)).thenReturn(savedArtist)
        assertEquals(savedArtist, artistBusiness.save(artist))

        verify(mockArtistRepository).findByName(artist.name!!)
        verify(mockArtistRepository).save(artist)
    }

    @Test
    fun `save without id name present`(){
        val artist = Artist("name", null)
        artist.webLinks = mutableListOf()

        val savedArtist = Artist("name", 1)
        savedArtist.webLinks = mutableListOf()

        Mockito.`when`(mockArtistRepository.findByName(artist.name!!)).thenReturn(Optional.of(savedArtist))
        assertEquals(savedArtist, artistBusiness.save(artist))

        verify(mockArtistRepository).findByName(artist.name!!)
    }

    @Test
    fun `save with id`(){
        val artist = Artist("name", 1)
        artist.webLinks = mutableListOf()

        val savedArtist = Artist("name", 1)
        savedArtist.webLinks = mutableListOf()

        Mockito.`when`(mockArtistRepository.save(artist)).thenReturn(savedArtist)
        assertEquals(savedArtist, artistBusiness.save(artist))

        verify(mockArtistRepository).save(artist)
    }

    @Test
    fun `save with DataIntegrityViolationException name present`(){
        val artist = Artist("name", 1)
        artist.webLinks = mutableListOf()

        val savedArtist = Artist("name", 1)
        savedArtist.webLinks = mutableListOf()

        Mockito.`when`(mockArtistRepository.findByName(artist.name!!)).thenReturn(Optional.of(savedArtist))
        Mockito.`when`(mockArtistRepository.save(artist))
            .thenThrow(DataIntegrityViolationException(""))
        assertEquals(savedArtist, artistBusiness.save(artist))

        verify(mockArtistRepository).inOrder {
            mockArtistRepository.save(artist)
            mockArtistRepository.findByName(artist.name!!)
        }
    }

    @Test
    fun `save with DataIntegrityViolationException name not present`(){
        val artist = Artist("name", 1)
        artist.webLinks = mutableListOf()

        val savedArtist = Artist("name", 1)
        savedArtist.webLinks = mutableListOf()

        Mockito.`when`(mockArtistRepository.save(artist))
            .thenThrow(DataIntegrityViolationException(""))
            .thenReturn(savedArtist)
        Mockito.`when`(mockArtistRepository.findByName(artist.name!!)).thenReturn(Optional.empty())
        assertEquals(savedArtist, artistBusiness.save(artist))

        verify(mockArtistRepository, times(2)).save(artist)
        verify(mockArtistRepository).findByName(artist.name!!)
    }

    @Test
    fun findById() {
        val artist = Artist("name", 1)

        /* Given */
        Mockito.`when`(mockArtistRepository.findById(artist.id!!)).thenReturn(Optional.of(artist))

        /* When */
        assertEquals(artist, artistBusiness.findById(artist.id!!).get())

        /* Then */
        verify(mockArtistRepository).findById(artist.id!!)
    }

    @Test
    fun `delete success`() {
        val artist = Artist("name", 1)

        /* Given */
        doNothing().`when`(mockArtistRepository).delete(artist)
        Mockito.`when`(mockArtistRepository.findById(artist.id!!)).thenReturn(Optional.empty())

        /* When */
        assertFalse(artistBusiness.delete(artist))

        /* Then */
        verify(mockArtistRepository).delete(artist)
    }

    @Test
    fun `delete error`() {
        val artist = Artist("name", 1)

        /* Given */
        doNothing().`when`(mockArtistRepository).delete(artist)
        Mockito.`when`(mockArtistRepository.findById(artist.id!!)).thenReturn(Optional.of(artist))

        /* When */
        assertTrue(artistBusiness.delete(artist))

        /* Then */
        verify(mockArtistRepository).delete(artist)
    }

    @Test
    fun getBooksByRole() {
        val artist = Artist("name", 1)
        val c1 = Contract()
        c1.book.isbn = "123456789123"
        c1.role.id = 1
        c1.artists = listOf(artist)
        c1.id = ContractId(1, "123456789123")
        val c2 = Contract()
        c2.book.isbn = "987654321987"
        c2.role.id = 2
        c2.artists = listOf(artist)
        c2.id = ContractId(2, "987654321987")
        val c3 = Contract()
        c3.book.isbn = "654123789456"
        c3.role.id = 1
        c3.artists = listOf(artist)
        c3.id = ContractId(1, "654123789456")

        artist.contracts = listOf(c1, c2, c3)

        val role1 = Role("role1", 1)
        val role2 = Role("role2", 2)

        val book1 = Book("123456789123")
        val book2 = Book("987654321987")
        val book3 = Book("654123789456")

        /* Given */
        Mockito.`when`(mockArtistRepository.findById(artist.id!!)).thenReturn(Optional.of(artist))
        Mockito.`when`(mockRoleBusiness.findById(1)).thenReturn(Optional.of(role1))
        Mockito.`when`(mockRoleBusiness.findById(2)).thenReturn(Optional.of(role2))
        Mockito.`when`(mockBookBusiness.getAllByIsbn(listOf("123456789123", "654123789456"))).thenReturn(listOf(book1, book2))
        Mockito.`when`(mockBookBusiness.getAllByIsbn(listOf("987654321987"))).thenReturn(listOf(book3))

        /* When */
        val list = artistBusiness.getBooksByRole(artist.id!!)
        assertEquals(2, list.size)
        val booksByRole1 = BooksByRole(role1, listOf(book1, book2))
        assertEquals(booksByRole1.books.size, list[0].books.size)
        assertEquals(booksByRole1.books[0], list[0].books[0])
        assertEquals(booksByRole1.books[1], list[0].books[1])
        assertEquals(booksByRole1.role, list[0].role)
        val booksByRole2 = BooksByRole(role2, listOf(book3))
        assertEquals(booksByRole2.books.size, list[1].books.size)
        assertEquals(booksByRole2.books[0], list[1].books[0])
        assertEquals(booksByRole2.role, list[1].role)
        /* Then */
        verify(mockArtistRepository).findById(artist.id!!)
        inOrder(mockRoleBusiness) {
            mockRoleBusiness.findById(1)
            mockRoleBusiness.findById(2)
        }
        inOrder(mockBookBusiness) {
            verify(mockBookBusiness).getAllByIsbn(listOf("123456789123", "654123789456"))
            verify(mockBookBusiness).getAllByIsbn(listOf("987654321987"))
        }
    }
}
