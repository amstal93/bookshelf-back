package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.api.v1.util.SearchOperation
import bzh.lautre.bookshelf.api.v1.util.SpecSearchCriteria
import bzh.lautre.bookshelf.model.Editor
import bzh.lautre.bookshelf.repository.EditorRepository
import bzh.lautre.bookshelf.specification.EditorSpecification
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.argThat
import org.mockito.kotlin.isNotNull
import org.mockito.kotlin.verify
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.jpa.domain.Specification
import java.util.*


@ExtendWith(MockitoExtension::class)
internal class EditorBusinessImplTest {
    @Mock
    lateinit var mockEditorRepository: EditorRepository

    @InjectMocks
    lateinit var editorBusiness: EditorBusinessImpl

    @Test
    fun allEditors() {
        val list = listOf(Editor("name", 1))

        /* Given */
        Mockito.`when`(mockEditorRepository.findAll()).thenReturn(list)

        /* When */
        assertEquals(list, editorBusiness.allEditors)
        /* Then */
        verify(mockEditorRepository).findAll()
    }

    @Test
    fun `search unpaged`() {
        val editor = Editor()
        val page = PageImpl(listOf(editor))
        val specification = EditorSpecification(SpecSearchCriteria("", "name", SearchOperation.EQUALITY, "batou"))

        /* Given */
        Mockito.lenient()
            .`when`(
                mockEditorRepository.findAll(
                    isNotNull(),
                    Mockito.any(Pageable::class.java)
                )
            )
            .thenReturn(page)

        /* When */
        assertEquals(editor, editorBusiness.search(specification, 0, 1, Sort.Direction.ASC, true).content[0])

        /* Then */
        verify(mockEditorRepository).findAll(
            argThat { spec: Specification<Editor> ->
                assertThat(spec).isNotNull
                true
            }, argThat { pageable: Pageable ->
                assertTrue(pageable.isUnpaged)
                true
            } as Pageable)
    }

    @Test
    fun `search paged`() {
        val editor = Editor()
        val page = PageImpl(listOf(editor))
        val specification = EditorSpecification(SpecSearchCriteria("", "name", SearchOperation.EQUALITY, "batou"))

        /* Given */
        Mockito.lenient()
            .`when`(
                mockEditorRepository.findAll(
                    isNotNull(),
                    Mockito.any(Pageable::class.java)
                )
            )
            .thenReturn(page)

        /* When */
        assertEquals(editor, editorBusiness.search(specification, 0, 1, Sort.Direction.ASC, false).content[0])

        /* Then */
        verify(mockEditorRepository).findAll(
            argThat { spec: Specification<Editor> ->
                assertThat(spec).isNotNull
                true
            }, argThat { pageable: Pageable ->
                assertTrue(pageable.isPaged)
                assertEquals(1, pageable.pageSize)
                assertEquals(0, pageable.pageNumber)
                true
            } as Pageable)
    }

    @Test
    fun `get by name doesn't exist`() {
        val editor = Editor("name", 1)
        /* Given */
        Mockito.`when`(mockEditorRepository.findByName("name")).thenReturn(Optional.empty())

        /* When */
        val savedEditor = editorBusiness.getByName("name")
        assertEquals(editor.name, savedEditor.name)
        assertNull(savedEditor.id)
        /* Then */
        verify(mockEditorRepository).findByName("name")
    }

    @Test
    fun `get by name exist`() {
        val editor = Editor("name", 1)

        /* Given */
        Mockito.`when`(mockEditorRepository.findByName(editor.name)).thenReturn(Optional.of(editor))

        /* When */
        assertEquals(editor, editorBusiness.getByName("name"))

        /* Then */
        verify(mockEditorRepository).findByName("name")
    }

    @Test
    fun `find by name exist`() {
        val editor = Editor("name", 1)

        /* Given */
        Mockito.`when`(mockEditorRepository.findByName(editor.name)).thenReturn(Optional.of(editor))

        /* When */
        val savedEditor = editorBusiness.getByName("name")
        assertEquals(1, savedEditor.id)
        assertEquals("name", savedEditor.name)

        /* Then */
        verify(mockEditorRepository).findByName("name")
    }

    @Test
    fun `save name not present`() {
        val editor = Editor("name  ", null)

        val savedEditor = Editor("name", 1)

        Mockito.`when`(mockEditorRepository.findByName(editor.name.trim())).thenReturn(Optional.empty())
        Mockito.`when`(mockEditorRepository.save(editor)).thenReturn(savedEditor)
        assertEquals(savedEditor, editorBusiness.save(editor))

        verify(mockEditorRepository).findByName(editor.name.trim())
        verify(mockEditorRepository).save(editor)
    }

    @Test
    fun `save name present`() {
        val editor = Editor("name  ", null)

        val savedEditor = Editor("name", 1)

        Mockito.`when`(mockEditorRepository.findByName(editor.name.trim())).thenReturn(Optional.of(savedEditor))
        assertEquals(savedEditor, editorBusiness.save(editor))

        verify(mockEditorRepository).findByName(editor.name.trim())
    }
}
