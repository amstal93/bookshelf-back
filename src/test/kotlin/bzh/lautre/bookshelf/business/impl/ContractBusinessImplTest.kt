package bzh.lautre.bookshelf.business.impl

import bzh.lautre.bookshelf.model.Contract
import bzh.lautre.bookshelf.model.ContractId
import bzh.lautre.bookshelf.repository.ContractRepository
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.verify
import java.util.*

@ExtendWith(MockitoExtension::class)
internal class ContractBusinessImplTest {
    @Mock
    lateinit var mockContractRepository: ContractRepository

    @InjectMocks
    lateinit var contractBusiness: ContractBusinessImpl


    @Test
    fun `save without id`(){
        val contract = Contract()

        val contractSavedId = ContractId(1, "1234567890123")
        val contractSaved = Contract()
        contractSaved.id = contractSavedId

        Mockito.`when`(mockContractRepository.save(contract)).thenReturn(contractSaved)
        assertEquals(contractSaved, contractBusiness.save(contract))

        verify(mockContractRepository).save(contract)
    }

    @Test
    fun `save with roleId in id`(){
        val contract = Contract()
        contract.book.isbn = "1234567890123"
        contract.role.id = 1

        val contractSavedId = ContractId(1, "1234567890123")
        val contractSaved = Contract()
        contractSaved.id = contractSavedId

        contract.id = contractSavedId

        Mockito.`when`(mockContractRepository.findById(Mockito.any(ContractId::class.java))).thenReturn(Optional.of(contractSaved))
        Mockito.`when`(mockContractRepository.save(contract)).thenReturn(contractSaved)
        assertEquals(contractSaved, contractBusiness.save(contract))

        verify(mockContractRepository).findById(Mockito.any(ContractId::class.java))
        verify(mockContractRepository).save(contract)
    }

    @Test
    fun findById() {
        val contractId = ContractId(1, "1234567890123")
        val contract = Contract()
        contract.id = contractId

        /* Given */
        Mockito.`when`(mockContractRepository.findById(contract.id)).thenReturn(Optional.of(contract))

        /* When */
        assertEquals(contract, contractBusiness.findById(contract.id).get())

        /* Then */
        verify(mockContractRepository).findById(contract.id)
    }


    @Test
    fun `delete success`() {
        val contractId = ContractId(1, "1234567890123")
        val contract = Contract()
        contract.id = contractId

        /* Given */
        doNothing().`when`(mockContractRepository).deleteById(contractId)
        Mockito.`when`(mockContractRepository.findById(contractId))
            .thenReturn(Optional.empty())

        /* When */
        assertFalse(contractBusiness.deleteById(contractId))

        /* Then */
        verify(mockContractRepository).deleteById(contractId)
    }

    @Test
    fun `delete error`() {
        val contractId = ContractId(1, "1234567890123")
        val contract = Contract()
        contract.id = contractId

        /* Given */
        doNothing().`when`(mockContractRepository).deleteById(contractId)
        Mockito.`when`(mockContractRepository.findById(contractId))
            .thenReturn(Optional.of(contract))

        /* When */
        assertTrue(contractBusiness.deleteById(contractId))

        /* Then */
        verify(mockContractRepository).deleteById(contractId)
    }
}
